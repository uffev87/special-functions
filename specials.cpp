#include <iostream>
#include <limits>
#include <complex>

// Program that computes complex exponential, logarithmic,
// trigonometric and hyperbolic functions.

const double inf = std::numeric_limits<double>::infinity(); // Assigning infinity.
const double NaN = std::numeric_limits<double>::quiet_NaN(); // Assigning undefined value.
const double tol = 1e-14; // Precision of approximations, should be small.
const double inv_tol = 1/tol; // Large value to guarantee all
                                // loops do enough iterations.
const double base = 0.5; // Used for logarithms. Should be 0 < base < 1.
                            // Rather close to 1 than close to 0.

int sgn(double x) {

    if (x < 0) {
        return -1;
    }

    else {
        return 1;
    }
}

double max(double* x) {

    int n = sizeof(x)/sizeof(int);

    double M = x[0];
    for (int i = 1; i < n; i++) {
        if (x[i] > M) {
            M = x[i];
        }
    }

    return M;
}

// exp(x) = sum(x^k/k!)

double taylor_exp(double x) {

    double abs_x = sgn(x) * x; // exp(x) = 1/exp(-x)

    double t = 1;
    double y = t;

    for (int i = 1; i <= inv_tol; i++) {
        t *= abs_x/i;
        y += t;

        if (t <= tol) {
            break;
        }
    }

    if (x < 0) {
        y = 1/y; // exp(x) = 1/exp(-x)
    }

    return y;
}

// cos(x) = sum((-1)^(k)x^(2k)/(2k)!)

double taylor_cos(double x) {

    double x_sqr = x * x;

    double p = 1;
    double m = 0.5 * x_sqr;
    double t = p - m;
    double y = t;

    for (int i = 4; i <= inv_tol; i+=4) {
        p = m * x_sqr/((i - 1) * i);
        m = p * x_sqr/((i + 1) * (i + 2));
        t = p - m;
        y += t;

        if (t <= tol) {
            break;
        }
    }

    return y;
}

// sin(x) = sum((-1)^(k)x^(2k + 1)/(2k + 1)!)

double taylor_sin(double x) {

    double x_sqr = x * x;

    double p = sgn(x) * x; // odd function
    double m = p * x_sqr/6;
    double t = p - m;
    double y = t;

    for (int i = 5; i <= inv_tol; i+=4) {
        p = m * x_sqr/((i - 1) * i);
        m = p * x_sqr/((i + 1) * (i + 2));
        t = p - m;
        y += t;

        if (t <= tol) {
            break;
        }
    }

    y *= sgn(x); // odd function;

    return y;
}

// ln(1 - x) = -sum(x^(k + 1)/(k + 1))

double taylor_ln(double x) {

    x = 1 - x;
    double k = x;
    double t = k;
    double y = -t;

    for (int i = 2; i <= inv_tol; i++) {
        k *= x;
        t = k/i;
        y -= t;

        if (t <= tol) {
            break;
        }
    }

    return y;
}

// arctan(x) = sum((-1)^(k)x^(2k + 1)/(2k + 1))

double taylor_arctan(double x) {

    double x_sqr = x * x;

    double p = sgn(x) * x; // odd function
    double m = p * x_sqr;
    double t = p - m/3;
    double y = t;

    for (int i = 5; i <= inv_tol; i+=4) {
        p = m * x_sqr;
        m = p * x_sqr;
        t = p/i - m/(i + 2);
        y += t;

        if (t <= tol) {
            break;
        }
    }

    y *= sgn(x); // odd function

    return y;
}

// pi/4 = 4arctan(1/5) - arctan(1/239)

const double pi_quarter = 4 * taylor_arctan(0.2) - taylor_arctan(1./239);
const double pi_half = 2 * pi_quarter;
const double pi = 4 * pi_quarter;

// exp(x) = (exp(x/k))^k, |x/k| < 1

double exp(double x) {

    if (x == -inf) {
        return 0;
    }

    else if (x == inf) {
        return inf;
    }

    int k = 0;

    while (sgn(x) * x > 1) {
        x *= 0.5;
        k += 1;
    }

    double y = taylor_exp(x);

    for (int i = 0; i < k; i++) {
        y *= y;
    }

    return y;
}

// cos(x) = cos(x - 2k * pi) = cos(y), |y| <= pi
// cos(x) = cos(y), |y| <= pi/2
// cos(x) = -sgn(y)sin(y - sgn(y)pi/2), |y| > pi

double cos(double x) {

    if (sgn(x) * x == inf) {
        return NaN;
    }

    double y;

    int k = x/pi;
    k = (k + sgn(k))/2;
    x -= 2 * k * pi;

    if (sgn(x) * x <= pi_half) {
        y = taylor_cos(x);
    }

    else {
        int sgn_x = sgn(x);
        x -= sgn_x * pi_half;
        y = -sgn_x * taylor_sin(x);
    }

    return y;
}

// sin(x) = sin(x - 2k * pi) = sin(y), |y| <= pi
// sin(x) = sin(y), |y| <= pi/2
// sin(x) = sgn(y)sin(y - sgn(y)pi/2), |y| > pi

double sin(double x) {

    if (sgn(x) * x == inf) {
        return NaN;
    }

    double y;

    int k = x/pi;
    k = (k + sgn(k))/2; // number of periods for x closest to 0
    x -= 2 * k * pi;

    if (sgn(x) * x <= pi_half) {
        y = taylor_sin(x);
    }

    else {
        int sgn_x = sgn(x);
        x -= sgn_x * pi_half;
        y = sgn_x * taylor_cos(x);
    }

    return y;
}

// ln(x) = ln(x/base^k) - k * ln(base), base < x/base^k <= 1

double ln(double x) {

    if (x == 0) {
        return -inf;
    }

    else if (x == inf) {
        return inf;
    }

    int k = 0;

    while (x > 1) {
        x *= base;
        k += 1;
    }

    while (x <= base) {
        x /= base;
        k -= 1;
    }

    double y = taylor_ln(x) - k * taylor_ln(base);

    return y;
}

// sqrt(y) = x, where x_(k + 1) = x_k + (x_k + y/x_k)/2,
// and x_0 is the initial guess of the solution x,
// and x_i approximates x iterativaly.

double iter_sqrt(double y, double x) {

    if (y == 0) {
        return 0;
    }

    else if (y == inf) {
        return inf;
    }

    double error = inf;

    while (sgn(error) * error > tol) {
        error = -x;
        x = 0.5 * (x + y/x);
        error += x;
    }

    return x;
}

// abs(a + bi) = sqrt(a² + b²)

double abs(std::complex<double> x) {

    double y = x.real() * x.real() + x.imag() * x.imag();

    double abs_re = sgn(x.real()) * x.real();
    double abs_im = sgn(x.imag()) * x.imag();

    static double vals[2] = {abs_re, abs_im};
    double x0 = max(vals);

    double xk = iter_sqrt(y, x0);

    return xk;
}

// arctan(x) = pi/2 - arctan(x) = sgn(x)(pi/4 - arctan((1 - |x|)/(1 + |x|)))
// |x| near 0  |x| being large    |x| near 1

double arctan(double x) {

    double sqrt_2 = iter_sqrt(2, 1.4);

    double y;

    if (sgn(x) * x <= 0.5 * sqrt_2) {
        y = taylor_arctan(x);
    }

    else if (sgn(x) * x >= sqrt_2) {
        y = sgn(x) * pi_half - taylor_arctan(1/x);
    }

    else {
        double z = (1 - sgn(x) * x)/(1 + sgn(x) * x);
        y = sgn(x) * (pi_quarter - taylor_arctan(z));
    }

    return y;
}

// atan2(b, a) = arg(a + bi)

double atan2(double im_x, double re_x) {

    double theta;

    if (re_x == 0 & im_x == 0) {
        theta = NaN;
    }

    else if (re_x > 0) {
        theta = arctan(im_x/re_x);
    }

    else if (re_x == 0) {
        theta = sgn(im_x) * pi_half;
    }

    else if (re_x < 0) {
        theta = arctan(im_x/re_x) + sgn(im_x) * pi;
    }

    return theta;
}

// exp(a + bi) = exp(a)cos(b) + exp(a)sin(b)i

std::complex<double> exp(std::complex<double> x) {

    double r = exp(x.real());

    double re_y = r * cos(x.imag());
    double im_y = r * sin(x.imag());

    std::complex<double> y(re_y, im_y);

    return y;
}

// cos(a + bi) = cos(a)cosh(b) - sin(a)sinh(b)i

std::complex<double> cos(std::complex<double> x) {

    double r = 0.5 * exp(x.imag());
    double rd = 0.25/r;

    double re_y = cos(x.real()) * (rd + r);
    double im_y = sin(x.real()) * (rd - r);

    std::complex<double> y(re_y, im_y);

    return y;
}

// sin(a + bi) = sin(a)cosh(b) + cos(a)sinh(b)i

std::complex<double> sin(std::complex<double> x) {

    double r = 0.5 * exp(x.imag());
    double rd = 0.25/r;

    double re_y = sin(x.real()) * (r + rd);
    double im_y = cos(x.real()) * (r - rd);

    std::complex<double> y(re_y, im_y);

    return y;
}

// tan(a + bi) = sin(a + bi)/cos(a + bi)

std::complex<double> complex_tan(std::complex<double> x) {

    double r = 0.5 * exp(x.imag());
    double rd = 0.25/r;

    double c = cos(x.real());
    double s = sin(x.real());
    double ch = r + rd;
    double sh = r - rd;

    double d = c * c * ch * ch + s * s * sh * sh;

    if (d == 0) {
        std::complex<double> y(inf, 0);

        return y;
    }

    double re_y = c * s/d;
    double im_y = ch * sh/d;

    std::complex<double> y(re_y, im_y);

    return y;
}

// ln(a + bi) = ln(abs(a + bi)) + arg(a + bi)i

std::complex<double> ln(std::complex<double> x) {

    double r = abs(x);

    double re_y = ln(r);
    double im_y = atan2(x.imag(), x.real());

    std::complex<double> y(re_y, im_y);

    return y;
}

// arccos(a + bi) = ln(a + bi + sqrt((a + bi)² - 1)/i

std::complex<double> arccos(std::complex<double> x) {

    double sgn_re = sgn(x.real());
    double sgn_im = sgn(x.imag());

    if (sgn_re * x.real() == inf) {
        std::complex<double> y((1 - sgn_re) * pi_half, inf);

        return y;
    }

    else if (sgn_im * x.imag() == inf) {
        std::complex<double> y(inf, (1 - sgn_re) * pi_half);

        return y;
    }

    else if (sgn_re * x.real() == 1 & x.imag() == 0) {
        std::complex<double> y((1 - sgn_re) * pi_half, 0);

        return y;
    }

    double re_x_sqr = x.real() * x.real();
    double im_x_sqr = x.imag() * x.imag();

    double re_val = re_x_sqr - im_x_sqr - 1;
    double im_val = 2 * x.real() * x.imag();
    std::complex<double> val(re_val, im_val);
    double abs_val = abs(val);

    double sqrt_val = iter_sqrt(abs_val, re_x_sqr + im_x_sqr + 1);
    double arg_val = 0.5 * atan2(im_val, re_val);

    double re = sqrt_val * cos(arg_val) + x.real();
    double im = sqrt_val * sin(arg_val) + x.imag();
    std::complex<double> z(re, im);
    double r = abs(z);

    double re_y = atan2(im, re);
    double im_y = -ln(r);

    std::complex<double> y(re_y, im_y);

    return y;
}

// arcsin(a + bi) = ln(-b + ai + sqrt(1 - (a + bi)²))/i

std::complex<double> arcsin(std::complex<double> x) {

    double sgn_re = sgn(x.real());
    double sgn_im = sgn(x.imag());

    if (sgn_re * x.real() == inf) {
        std::complex<double> y(sgn_re * pi_half, inf);

        return y;
    }

    else if (sgn_im * x.imag() == inf) {
        std::complex<double> y(inf, sgn_im * pi_half);

        return y;
    }

    else if (sgn_re * x.real() == 1 & x.imag() == 0) {
        std::complex<double> y(sgn_re * pi_half, 0);

        return y;
    }

    double re_x_sqr = x.real() * x.real();
    double im_x_sqr = x.imag() * x.imag();

    double re_val = 1 + im_x_sqr - re_x_sqr;
    double im_val = -2 * x.real() * x.imag();
    std::complex<double> val(re_val, im_val);
    double abs_val = abs(val);

    double sqrt_val = iter_sqrt(abs_val, re_x_sqr + im_x_sqr + 1);
    double arg_val = 0.5 * atan2(im_val, re_val);

    double re = sqrt_val * cos(arg_val) - x.imag();
    double im = sqrt_val * sin(arg_val) + x.real();
    std::complex<double> z(re, im);
    double r = abs(z);

    double re_y = atan2(im, re);
    double im_y = -ln(r);

    std::complex<double> y(re_y, im_y);

    return y;
}

// arctan(a + bi) = ln((i - (a + bi))/(i + (a + bi)))/2i

std::complex<double> arctan(std::complex<double> x) {

    double sgn_im = sgn(x.imag());

    if (x.real() == 0 & sgn_im * x.imag() == 1) {
        std::complex<double> y(NaN, sgn_im * inf);

        return y;
    }

    double re_x_sqr = x.real() * x.real();
    double im1_x = x.imag() + 1;
    double d = re_x_sqr + im1_x * im1_x;

    double re = (1 - re_x_sqr - x.imag() * x.imag())/d;
    double im = 2 * x.real()/d;
    std::complex<double> z(re, im);
    double abs_x = abs(z);

    double re_y = 0.5 * atan2(im, re);
    double im_y = -0.5 * ln(abs_x);

    std::complex<double> y(re_y, im_y);

    return y;
}

// (a + bi)(c + di) = ac - bd + (ad + bc)i

std::complex<double> complex_multiplier(std::complex<double> x1, std::complex<double> x2) {

    double re = x1.real() * x2.real() - x1.imag() * x2.imag();
    double im = x1.real() * x2.imag() + x1.imag() * x2.real();

    std::complex<double> y(re, im);

    return y;
}

// 1/(a + bi) = (a - bi)/(a² + b²)

std::complex<double> complex_divider(std::complex<double> x) {

    double d = x.real() * x.real() + x.imag() * x.imag();

    if (d == 0) {
        std::complex<double> y(inf, NaN);

        return y;
    }

    double re_y = x.real()/d;
    double im_y = -x.imag()/d;

    std::complex<double> y(re_y, im_y);

    return y;
}

// Formatting complex(a, b) to either a, a - bi or a + bi

void complex_format(std::complex<double> x) {

    std::cout << x.real();
    if (x.imag() > 0) {
        std::cout << " + " << x.imag() << "i";
    }
    else if (x.imag() < 0) {
        std::cout << " - " << -x.imag()<< "i";
    }
}

int main()
{
    double re_x;
    double im_x;
    std::string f;

    std::cout << "Input real part of x" << std::endl;
    std::cin >> re_x;

    std::cout << "Input imaginary part of x" << std::endl;
    std::cin >> im_x;

    std::cout << "What function would you like to compute? Type," << std::endl;
    std::cout << "exp, for the exponential function." << std::endl;
    std::cout << "cos, sec, sin, csc, tan or cot for trigonometric functions." << std::endl;
    std::cout << "cosh, sech, sinh, csch, tanh or coth for hyperbolic functions." << std::endl;
    std::cout << "ln, for the natural logarithm." << std::endl;
    std::cout << "arccos, arcsec, arcsin, arccsc, arctan or arccot for inverse trigonometric functions." << std::endl;
    std::cout << "arccosh, arcsech, arcsinh, arccsch, arctanh or arccoth for inverse hyperbolic functions." << std::endl;
    std::cout << "Formats such as log, instead of ln, or, ar or inv, instead of arc can also be used." << std::endl;
    std::cin >> f;
    std::cout << std::endl;

    std::complex<double> x(re_x, im_x);
    std::complex<double> y;

    if (f == "exp") {
        y = exp(x);
    }

    else if (f == "cos" | f == "sec") {
        y = cos(x);
        if (f == "sec") {
            // sec(a + bi) = 1/cos(a + bi)
            y = complex_divider(y);
        }
    }

    else if (f == "sin" | f == "csc") {
        y = sin(x);
        if (f == "csc") {
            // csc(a + bi) = 1/sin(a + bi)
            y = complex_divider(y);
        }
    }

    else if (f == "tan" | f == "cot") {
        y = complex_tan(x);
        if (f == "cot") {
            // cot(a + bi) = 1/tan(a + bi)
            y = complex_divider(y);
        }
    }

    else if (f == "cosh" | f == "sech") {
        // cosh(a + bi) = cos(-b + ai)
        std::complex<double> z(-x.imag(), x.real());
        y = cos(z);
        if (f == "sech") {
            // sech(a + bi) = 1/cosh(a + bi)
            y = complex_divider(y);
        }
    }

    else if (f == "sinh" | f == "csch") {
        // sinh(a + bi) = -sin(-b + ai)i
        std::complex<double> z(-x.imag(), x.real());
        std::complex<double> m1(0, -1);
        std::complex<double> m2 = sin(z);
        y = complex_multiplier(m1, m2);
        if (f == "csch") {
            // csch(a + bi) = 1/sinh(a + bi)
            y = complex_divider(y);
        }
    }

    else if (f == "tanh" | f == "coth") {
        // tanh(a + bi) = -tan(-b + ai)i
        std::complex<double> z(-x.imag(), x.real());
        std::complex<double> m1(0, -1);
        std::complex<double> m2 = tan(z);
        y = complex_multiplier(m1, m2);
        if (f == "coth") {
            // coth(a + bi) = 1/tanh(a + bi)
            y = complex_divider(y);
        }
    }

    else if (f == "ln" | f == "log") {
        f = "ln";
        y = ln(x);
    }

    else if (f == "arccos" | f == "arcos" | f == "invcos") {
        f = "arccos";
        y = arccos(x);
        y *= sgn(y.real());
    }

    else if (f == "arcsec" | f == "arsec" | f == "invsec") {
        f = "arcsec";
        // arcsec(a + bi) = cos(1/(a + bi))
        y = complex_divider(x);
        y = arccos(y);
        y *= sgn(y.real());
    }

    else if (f == "arcsin" | f == "arsin" | f == "invsin") {
        f = "arcsin";
        y = arcsin(x);
    }

    else if (f == "arccsc" | f == "arcsc" | f == "invcsc") {
        f = "arccsc";
        // arccsc(a + bi) = sin(1/(a + bi))
        y = complex_divider(x);
        y = arcsin(y);
    }

    else if (f == "arctan" | f == "artan" | f == "invtan") {
        f = "arctan";
        y = arctan(x);
    }

    else if (f == "arccot" | f == "arcot" | f == "invcot") {
        f = "arccot";
        // arccot(a + bi) = pi - arctan(a + bi)
        y = arctan(x);
        y = sgn(y.real()) * pi_half - y;
    }

    else if (f == "arccosh" | f == "arcosh" | f == "invcosh") {
        f = "arccosh";
        // arccosh(a + bi) = arccos(a + bi)i
        std::complex<double> m1(0, 1);
        std::complex<double> m2 = arccos(x);
        y = complex_multiplier(m1, m2);
    }

    else if (f == "arcsech" | f == "arsech" | f == "invsech") {
        f = "arcsech";
        // arcsech(a + bi) = arccosh(1/(a + bi))
        std::complex<double> m1(0, 1);
        std::complex<double> m2 = complex_divider(x);
        m2 = arccos(m2);
        y = complex_multiplier(m1, m2);
    }

    else if (f == "arcsinh" | f == "arsinh" | f == "invsinh") {
        f = "arcsinh";
        // arcsinh(a + bi) = arcsin(b - ai)i
        std::complex<double> m1(0, 1);
        std::complex<double> z(x.imag(), -x.real());
        std::complex<double> m2 = arcsin(z);
        y = complex_multiplier(m1, m2);
    }

    else if (f == "arccsch" | f == "arcsch" | f == "invcsch") {
        f = "arccsch";
        // arccsch(a + bi) = arcsinh(1/(a + bi))
        std::complex<double> m1(0, 1);
        std::complex<double> zd = complex_divider(x);
        std::complex<double> z(zd.imag(), -zd.real());
        std::complex<double> m2 = arcsin(z);
        y = complex_multiplier(m1, m2);
    }

    else if (f == "arctanh" | f == "artanh" | f == "invtanh") {
        f = "arctanh";
        // arctanh(a + bi) = arctan(b - ai)i
        std::complex<double> m1(0, 1);
        std::complex<double> z(x.imag(), -x.real());
        std::complex<double> m2 = arctan(z);
        if (sgn(m2.imag()) * m2.imag() == inf) {
            std::complex<double> t(inf, NaN);
            y = t;
        }
        else {
            y = complex_multiplier(m1, m2);
        }
    }

    else if (f == "arccoth" | f == "arcoth" | f == "invcoth") {
        f = "arccoth";
        // arccoth(a + bi) = arctanh(a + bi) - (pi/2)i
        std::complex<double> m1(0, 1);
        std::complex<double> z(x.imag(), -x.real());
        std::complex<double> m2 = arctan(z);
        if (sgn(m2.imag()) * m2.imag() == inf) {
            std::complex<double> t(inf, NaN);
            y = t;
        }
        else {
            std::complex<double> p(0, pi_half);
            y = complex_multiplier(m1, m2) - p;
        }
    }

    std::cout << f << "(";
    complex_format(x);
    std::cout << ") = ";
    complex_format(y);

    if (f == "ln") {

        if (re_x == 0 & im_x == 0) {
            std::cout << " + nani";
        }

        else {
            std::cout << " + (2k * pi)i, for all integers, k";
        }
    }

    else if (f == "arccos" | f == "arcsec" | f == "arcsin" | f == "arccsc") {

        std::cout << " + 2k * pi, for all integers, k," << std::endl;
        std::cout << "or, ";

        // arccos(a + bi) = -arccos(a + bi)
        if (f == "arccos" | f == "arcsec") {
            complex_format(-y);
        }

        // arcsin(a + bi) = pi - arcsin(a + bi)
        else if (f == "arcsin" | f == "arccsc") {
            y = sgn(y.real()) * pi - y;
            complex_format(y);
        }

        std::cout << " + 2k * pi, for all integers, k";
    }

    else if (f == "arctan" | f == "arccot") {

        if (sgn(y.imag()) * y.imag() != inf) {
            std::cout << " + k * pi, for all integers, k";
        }
    }

    else if (f == "arccosh" | f == "arcsech" | f == "arcsinh" | f == "arccsch") {

        std::cout << " + (2k * pi)i, for all integers, k," << std::endl;
        std::cout << "or, ";

        // arccosh(a + bi) = -arccosh(a + bi)
        if (f == "arccosh" | f == "arcsech") {
            complex_format(-y);
        }

        // arcsinh(a + bi) = pi*i - arcsinh(a + bi)
        else if (f == "arcsinh" | f == "arccsch") {
            std::complex<double> z(0, sgn(y.imag()) * pi);
            complex_format(z - y);
        }

        std::cout << " + (2k * pi)i, for all integers, k";
    }

    else if (f == "arctanh" | f == "arccoth") {

        if (sgn(y.real()) * y.real() == inf) {
            std::cout << " + nani";
        }

        else {
            std::cout << " + (k * pi)i, for all integers, k";
        }
    }

    std::cout << std::endl;

    return 0;
}
